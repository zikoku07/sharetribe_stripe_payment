class AddStripeChargeFiledToPaymentSettings < ActiveRecord::Migration
  def change
    add_column :payment_settings, :stripe_charge, :float
    add_column :payment_settings, :stripe_transaction_fee, :float
  end
end
