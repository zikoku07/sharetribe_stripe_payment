# == Schema Information
#
# Table name: stripe_accounts
#
#  id           :integer          not null, primary key
#  person_id    :string(255)
#  community_id :integer
#  account_id   :string(255)
#  token        :text(65535)
#  public_key   :text(65535)
#  secret_key   :text(65535)
#  state        :string(255)
#  country      :string(255)
#  active       :boolean          default(FALSE)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe StripeAccount, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
