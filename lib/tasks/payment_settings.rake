namespace :community_payment_settings do
  desc "Sends the community updates email to everyone who should receive it now"
  task :payment_setting => :environment do |t, args|
    community = Community.last
    PaymentSettings.create(community_id: community.id, active: true, payment_gateway: 'stripe',payment_process: 'preauthorize' ,confirmation_after_days: 10)
  end
end